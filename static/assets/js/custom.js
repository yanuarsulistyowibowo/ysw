var App = (function(window){
  "use strict";
  var _this = null;
  var cacheCollection = {};
  return{
    init : function(){
      _this = this; 

      this.SearchPopUp();

      this.SideMenu();

      this.BlogLoadMore();

      $(window).resize(function () {
        var width = $(window).width();
        if(width > 767){
          $("div.s-menu").removeClass("s-menu_toggle");
        }
      });

      $(document).click(function(e) {
        if (!$(e.target).is('.s-menu_icon, .s-menu *')) {
           $("div.s-menu").removeClass("s-menu_toggle");
        }
      });

    },

    getObject: function(selector){
      if(typeof cacheCollection[selector] == "undefined"){
        cacheCollection[selector] = $(selector);
      }
      return cacheCollection[selector];
    },

    SearchPopUp: function() {
      $(".s-search_icon").on("click", function() {
        $(".s-search_box").toggleClass("s-search_toggle");
      });
    },

    SideMenu: function() {
      $("#s-menu_icon").on("click", function() {
        $("div.s-menu").toggleClass("s-menu_toggle");
      });
    },

    BlogLoadMore: function(){
      var mnm = ".s-show_more a";
      var mnl = "#s-blog_listing > div";
      var mn = "#s-blog_listing";
      _this.getObject(mnm).on("click",function(){ 
        var i = 0;
        var n = _this.getObject(mnl).length; 
        var j = 0; 
        var data_show = $(mn).attr("data-show"); 
        _this.getObject(mnl).each(function( index ) { 

          var data_attr = $(this).attr("data-attr"); 
          if(data_attr == 0){
            if(i < data_show){
              $(this).fadeIn( "slow");
              $(this).attr("data-attr","1");
            }
            i++;
          }
          if(data_attr == 1){
            j++;
          }
        }); 
        var total_show = parseInt(j)+parseInt(data_show);
        if(total_show >= n){
          _this.getObject(mnm).addClass("disable");
        }
      });
    } 
  }
})(window);


$(document).ready(function($) {
  App.init();
});