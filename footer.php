<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ysw
 */

?>

<footer>
  <div class="s-footer clearfix row">
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	  <span class="s-small_title">Status</span>
	  <p>
		Available for projects, talks & <br>workshops.
	  </p>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	  <span class="s-small_title">Socialising</span>
	  <p>
		<a href="index.html#"><i class="fa fa-linkedin"></i></a>
		<a href="index.html#"><i class="fa fa-twitter"></i></a>
		<a href="index.html#"><i class="fa fa-behance"></i></a>
		<a href="index.html#"><i class="fa fa-dribbble"></i></a>
	  </p>
	</div>
  </div>
  <p class="y-copyright">Copyright © <?php echo date('Y'); ?> Yanuar Sulistyo Wibowo
  <!--<a href="mailto:yanuar@ysw.web.id"><u>yanuar@ysw.web.id</u></a>--></p>
</footer>
</div>
</div>

<!-- JQUERY:: JQUERY.JS -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/jquery.min.js"></script>

<!-- JQUERY:: CUSTOM.JS -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/custom.js"></script>

<?php wp_footer(); ?>

</body>
</html>
