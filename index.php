<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ysw
 */

get_header(); ?>

<div class="s-intro_home">
  <h1>
	Hello, I’m Yanuar, <br>
	A Web & WordPress Developer <br>
	From Jakarta.
  </h1>
</div>
<div class="s-work_listing">
  <span class="s-small_title">Selected works</span>
  <div class="row clearfix">
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	  <div class="s-work_intro">
		<img src="<?php bloginfo('template_url'); ?>/assets/images/home_01.jpg" class="img-responsive" alt="">
		<div class="s-work_intro_text">
		  <a href="project.html">
			<span class="s-small_title">Panels Dashboard</span>
			<p>
			  UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
			</p>
		  </a>
		</div>
	  </div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	  <div class="s-work_intro">
		<img src="<?php bloginfo('template_url'); ?>/assets/images/home_06.jpg" class="img-responsive" alt="">
		<div class="s-work_intro_text">
		  <a href="project.html">
			<span class="s-small_title">Panels Dashboard</span>
			<p>
			  UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
			</p>
		  </a>
		</div>
	  </div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	  <div class="s-work_intro">
		<img src="<?php bloginfo('template_url'); ?>/assets/images/home_02.jpg" class="img-responsive" alt="">
		<div class="s-work_intro_text">
		  <a href="project.html">
			<span class="s-small_title">Panels Dashboard</span>
			<p>
			  UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
			</p>
		  </a>
		</div>
	  </div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	  <div class="s-work_intro">
		<img src="<?php bloginfo('template_url'); ?>/assets/images/home_03.jpg" class="img-responsive" alt="">
		<div class="s-work_intro_text">
		  <a href="project.html">
			<span class="s-small_title">Panels Dashboard</span>
			<p>
			  UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
			</p>
		  </a>
		</div>
	  </div>
	</div>
  </div>
</div>
<div class="s-work_listing">
  <span class="s-small_title">For Sale</span>
  <div class="row clearfix">
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	  <div class="s-work_intro">
		<img src="<?php bloginfo('template_url'); ?>/assets/images/home_04.jpg" class="img-responsive" alt="">
		<div class="s-work_intro_text">
		  <a href="project.html">
			<span class="s-small_title">Panels Dashboard</span>
			<p>
			  UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
			</p>
		  </a>
		</div>
	  </div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	  <div class="s-work_intro">
		<img src="<?php bloginfo('template_url'); ?>/assets/images/home_05.jpg" class="img-responsive" alt="">
		<div class="s-work_intro_text">
		  <a href="project.html">
			<span class="s-small_title">Panels Dashboard</span>
			<p>
			  UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
			</p>
		  </a>
		</div>
	  </div>
	</div>
  </div>
</div>

<?php
//get_sidebar();
get_footer();
