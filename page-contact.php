<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ysw
 */

get_header(); ?>

<div class="clearfix">
        <h1>
          Do you want to work <br>
          with me? Don’t hesitate <br>
          to contact me.
        </h1>
        <div class="row clearfix s-contact">
          <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <span class="s-small_title">Let’s get to work!</span>
            <form action="contact.html#">
              <div class="clearfix s-input_half">
                <input type="text" placeholder="Full Name">
                <input type="email" placeholder="Email">
              </div>
              <input type="text" placeholder="Subject">
              <textarea placeholder="Message"></textarea>
              <input type="submit" value="Send Message">
            </form>
          </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 pull-right s-contact_info">
            <p>
              <strong>From:</strong>
              <i class="fa fa-star"></i> Jakarta
            </p>
            <p>
              <strong>Skype:</strong>
              yanuarsulistyowibowo
            </p>
            <p>
              <strong>Email::</strong>
              <a href="mailto:yanuar@ysw.web.id">yanuar@ysw.web.id</a>
            </p>
          </div>
        </div>
     </div>

<?php
get_footer();