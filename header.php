<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ysw
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<!-- PAGE TITLE -->
<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>

<!-- META-DATA -->
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<meta name="description" content="" >
<meta name="keywords" content="" >

<!-- CSS:: FONTS -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome.min.css">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>

<!-- CSS:: BOOTSTRAP -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css">

<!-- CSS:: ANIMATION -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/animate.css">

<!-- CSS:: -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/reset.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/main.css">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<div class="s-wrapper">
	  <div class="container">
		<header>
		  <div class="row clearfix s-header">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			  <a href="<?php bloginfo('url'); ?>" class="s-logo"><?php bloginfo('name'); ?></a>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
			  <div class="s-menu pull-right">
				<nav>
				  <!-- <ul>
					<li class="s-active"><a href="index.html">Home</a></li>
					<li><a href="about.html">About</a></li>
					<li><a href="portfolio.html">portfolio</a></li>
					<li><a href="blog.html">Blog</a></li>
					<li><a href="contact.html">Contact</a></li>
				  </ul> -->
				  <?php wp_nav_menu( array( 'menu' => 'Main Menu', 'depth' => 2, 'container' => 'false')); ?>
				</nav>
			  </div>
			  <i class="fa fa-bars s-menu_icon" id="s-menu_icon" aria-hidden="true"></i>
			</div>
		  </div>
		</header>
