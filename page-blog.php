<?php
/*
Template Name: Blog Posts
*/

get_header(); ?>

    <div class="s-blog_listing">
       <div class="s-blog_single">
            <span class="s-small_title"><a href="blog_single.html">Blog</a></span>
            <p>
                The online journal of Sofia Porter; contains information about new and upcoming projects, events she has attended,<br>development rants and a rather large sum of posts about cool things she wants but cant have. Last blog post was on<br> Tuesday, December 8, 2016.
            </p>
       </div> 

        <?php query_posts('post_type=post&post_status=publish&offset=0&showposts=1&paged='. get_query_var('paged')); ?>

        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

        <div class="s-blog_single s-feature_post">
            <img src="<?php bloginfo('template_url'); ?>/assets/images/blog_01.jpg" class="img-responsive" alt="">
            <div class="s-feature_text">
                <h2>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h2>
                <span><?php echo get_the_date('M d, Y'); ?> / <u>2 comments</u></span>
            </div>
        </div>

        <?php endwhile; endif; ?>

        <?php wp_reset_query(); ?>

        <!-- <div class="row clearfix s-blog_listing" id="s-blog_listing"> -->
        <div class="row clearfix">

            <?php query_posts('post_type=post&post_status=publish&offset=1&showposts=9&paged='. get_query_var('paged')); ?>
            <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/blog_02.jpg" class="img-responsive" alt=""></a>
                <div class="s-blog_info">
                    <span><?php echo get_the_date('M d, Y'); ?> / <u>2 comments</u></span>
                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                </div>
            </div>

            <?php endwhile; endif; ?>
            <?php wp_reset_query(); ?>
        </div>
<!-- ysw -->
     </div> 

<?php
get_footer();