<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ysw
 */

get_header(); ?>

<div class="s-intro_home">
       <h1>
         Identifying, clarifying and<br> 
         solving problems — making UI design <br>solutions with a purpose.
       </h1>
     </div>
     <div class="s-work_listing">
       <span class="s-small_title">Selected works</span>
       <div class="row clearfix">
         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           <div class="s-work_intro">
             <img src="<?php bloginfo('template_url'); ?>/assets/images/portfolio_01.jpg" class="img-responsive" alt="">
             <div class="s-work_intro_text">
               <a href="project.html">
                 <span class="s-small_title">Panels Dashboard</span>
                 <p>
                   UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
                 </p>
               </a>
             </div>
           </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           <div class="s-work_intro">
             <img src="<?php bloginfo('template_url'); ?>/assets/images/portfolio_02.jpg" class="img-responsive" alt="">
             <div class="s-work_intro_text">
               <a href="project.html">
                 <span class="s-small_title">Panels Dashboard</span>
                 <p>
                   UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
                 </p>
               </a>
             </div>
           </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           <div class="s-work_intro">
             <img src="<?php bloginfo('template_url'); ?>/assets/images/portfolio_03.jpg" class="img-responsive" alt="">
             <div class="s-work_intro_text">
               <a href="project.html">
                 <span class="s-small_title">Panels Dashboard</span>
                 <p>
                   UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
                 </p>
               </a>
             </div>
           </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           <div class="s-work_intro">
             <img src="<?php bloginfo('template_url'); ?>/assets/images/portfolio_08.jpg" class="img-responsive" alt="">
             <div class="s-work_intro_text">
               <a href="project.html">
                 <span class="s-small_title">Panels Dashboard</span>
                 <p>
                   UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
                 </p>
               </a>
             </div>
           </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           <div class="s-work_intro">
             <img src="<?php bloginfo('template_url'); ?>/assets/images/portfolio_08.jpg" class="img-responsive" alt="">
             <div class="s-work_intro_text">
               <a href="project.html">
                 <span class="s-small_title">Panels Dashboard</span>
                 <p>
                   UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
                 </p>
               </a>
             </div>
           </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           <div class="s-work_intro">
             <img src="<?php bloginfo('template_url'); ?>/assets/images/portfolio_08.jpg" class="img-responsive" alt="">
             <div class="s-work_intro_text">
               <a href="project.html">
                 <span class="s-small_title">Panels Dashboard</span>
                 <p>
                   UI/UX design for Exposure DB - it is a real-time analytics platform for offline marketing and event industry professionals.
                 </p>
               </a>
             </div>
           </div>
         </div>
       </div>
     </div>
     

<?php
get_footer();