<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ysw
 */

get_header(); ?>

<div class="s-portfolio"> 
         <div class="s-large">
           <img src="<?php bloginfo('template_url'); ?>/assets/images/about_01.jpg" class="img-responsive" alt="">
         </div>
         <div class="clearfix">
           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="row">
             <img src="<?php bloginfo('template_url'); ?>/assets/images/about_02.jpg" class="img-responsive" alt="">
             <img src="<?php bloginfo('template_url'); ?>/assets/images/about_03.jpg" class="img-responsive" alt="">
            </div>
           </div>
           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
             <div class="row">
               <img src="<?php bloginfo('template_url'); ?>/assets/images/about_04.jpg" class="img-responsive" alt="">
             </div>
           </div>
         </div> 
     </div>
     <div class="s-about row clearfix">
       <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
           <span class="s-small_title">About</span>
           <p>
             Sofia Porter is a freelance designer and for the past eight years she's worked for some of the Russian best digital agencies; producing high quality work wherever he's been.
           </p>
           <p>
             Working at the very highest level as both a senior designer, Sofia provides her experience, knowledge and expertise to clients offering them something more than your average freelancer.
           </p>
         </div>
         <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
           <span class="s-small_title">Interests</span>
           <ul> 
             <li><a href="about.html#">Art</a></li>
             <li><a href="about.html#">Cooking</a></li>
             <li><a href="about.html#">Design</a></li>
             <li><a href="about.html#">Films and entertainment</a></li>
             <li><a href="about.html#">History</a></li>
             <li><a href="about.html#">Languages</a></li>
             <li><a href="about.html#">Psychology</a></li>
             <li><a href="about.html#">Travel</a></li>
           </ul>
         </div>
         <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
           <span class="s-small_title">Skills</span>
           <ul> 
             <li><a href="about.html#">Adobe Photoshop</a></li>
             <li><a href="about.html#">Adobe Illustrator</a></li>
             <li><a href="about.html#">Adobe InDesign</a></li>
             <li><a href="about.html#">HTML/HTML5</a></li>
             <li><a href="about.html#">CSS/SCSS</a></li>
             <li><a href="about.html#">JS</a></li>
             <li><a href="about.html#">Wordpress</a></li>
             <li><a href="about.html#">Tumblr</a></li>
           </ul>
         </div> 
     </div>

<?php
get_footer();